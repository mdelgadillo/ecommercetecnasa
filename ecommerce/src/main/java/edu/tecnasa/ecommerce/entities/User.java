package edu.tecnasa.ecommerce.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="E_USER")
public class User implements Identifiable,Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_USER")
	@JsonProperty("UserId")
	private Long Id;
	
	@Column(length=255,nullable=false,unique=true)
	@JsonProperty("Username")
	private String username;
	
	@Column(length=150,nullable=false)
	@JsonIgnore
	private String password;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="E_USER_CLAIM",
	joinColumns=@JoinColumn(name="ID_USER"),
	inverseJoinColumns=@JoinColumn(name="CLAIM_TYPE"))
	private Set<ClaimType> claims;
	
	public Set<ClaimType> getClaims() {
		return claims;
	}
	public void setClaims(Set<ClaimType> claims) {
		this.claims = claims;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
