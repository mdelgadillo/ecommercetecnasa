package edu.tecnasa.ecommerce.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import edu.tecnasa.ecommerce.entities.Product;
import org.springframework.data.repository.query.Param;

@Repository
public interface ProductDao extends PagingAndSortingRepository<Product,Long>{
	
	@Query("SELECT this FROM Product this WHERE this.category.id = :idCategory")
	Page<Product> searchByCategoryId(Pageable pageable, @Param("idCategory") Long idCategory);
	
}
