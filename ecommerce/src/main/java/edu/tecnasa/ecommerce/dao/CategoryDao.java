package edu.tecnasa.ecommerce.dao;

import org.springframework.stereotype.Repository;
import edu.tecnasa.ecommerce.entities.Category;
import org.springframework.data.repository.PagingAndSortingRepository;

@Repository
public interface CategoryDao extends PagingAndSortingRepository<Category,Long>{

}
