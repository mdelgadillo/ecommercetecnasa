package edu.tecnasa.ecommerce.dao;
import org.springframework.stereotype.Repository;
import edu.tecnasa.ecommerce.entities.User;
import org.springframework.data.repository.PagingAndSortingRepository;

@Repository
public interface UserDao extends PagingAndSortingRepository<User,Long> {
	public User findByUsername(String username);
}
