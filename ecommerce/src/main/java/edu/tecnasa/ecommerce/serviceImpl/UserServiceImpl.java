package edu.tecnasa.ecommerce.serviceImpl;

import javax.inject.Inject;
import java.util.List;
import java.util.ArrayList;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import edu.tecnasa.ecommerce.dao.UserDao;
import edu.tecnasa.ecommerce.entities.ClaimType;
import edu.tecnasa.ecommerce.entities.User;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Inject
	private UserDao userDao;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User currentUser=userDao.findByUsername(username);
		if(currentUser==null) {
			throw new UsernameNotFoundException("User Not Found");			
		}
		List<GrantedAuthority> grantedAuthorities=new ArrayList<>();
		if(currentUser.getClaims()!=null) {
			for(ClaimType claimType: currentUser.getClaims()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(claimType.getClaimType()));
			}
		}
		UserDetails user =new org.springframework.security.core.userdetails.User(username, currentUser.getPassword(), grantedAuthorities);
		return user;
	}

}
